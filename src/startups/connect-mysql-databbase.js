const Knex = require('knex');
const { Model, knexSnakeCaseMappers } = require('objection');

module.exports = async ({ host, port, database, user, password }) => {
  let knex = Knex({
    client: 'mysql',
    connection: {
      host: host,
      user: user,
      password: password,
      port: port,
      database: database,
    },
    pool: { min: 0, max: 10 },
    ...knexSnakeCaseMappers(),
  });

  Model.knex(knex);
};
