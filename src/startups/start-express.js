const express = require('express');

const salesRouter = require('../routers/sales-router');
const errorMiddleware = require('../middlewares/error-middleware');

module.exports = ({ port }) => {
  const app = express();

  app.use(errorMiddleware);
  app.use(express.json());
  app.use('/sales', salesRouter);

  app.listen(port, () => {
    console.log(`Listening to port ${process.env.PORT}`);
  });
};
