const schedule = require('node-schedule');

const triggerExternalEndpoint = require('../tasks/trigger-external-endpoint');

module.exports = ({ externalEndpointUrl }) => {
  if (externalEndpointUrl) {
    schedule.scheduleJob('0 0 * * * *', async () => {
      await triggerExternalEndpoint(externalEndpointUrl);
    });
  } else {
    console.warn('EXTERNAL_ENDPOINT_URL not set, schedule not started.');
  }
};
