const axios = require('axios');

module.exports = async (externalEndpointUrl) => {
  console.log('Triggering external endpoint...');
  try {
    await axios.get(externalEndpointUrl);
  } catch (e) {
    console.log('Failed triggering external endpoint');
  }
};
