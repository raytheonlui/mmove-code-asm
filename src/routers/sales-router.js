const express = require('express');
const multer = require('multer');
const router = express.Router();

const controller = require('../controllers/sales-controller');

router.post('/record', multer({ dest: 'tmp/' }).single('file'), controller.record);
router.get('/report', controller.report);

module.exports = router;
