const fs = require('fs-extra');
const validateCsvFile = require('./validate-csv-file');
const insertCsvRecords = require('./insert-csv-records');

module.exports = async (req, res) => {
  if (!req.file || !req.file.path) {
    res.status(401);
    res.send({
      ok: false,
      message: 'file is not provided',
    });
    return;
  }

  const filePath = req.file.path;

  try {
    // read the csv in 2 passes to avoid storing all the records in limited RAM
    await validateCsvFile(filePath);
    await insertCsvRecords(filePath);

    res.status(200);
    res.send({
      ok: true,
    });
  } catch (e) {
    res.status(401);
    res.send({
      ok: false,
      message: e,
    });
  } finally {
    await fs.unlink(filePath);
  }
};
