const fs = require('fs-extra');
const csvParser = require('csv-parser');
const moment = require('moment');
const SalesRecord = require('../../../models/SalesRecord');


// Insert all the records in a transaction, so the DB will hold either all or none of the records.
module.exports = async (path) => {
  const trx = await SalesRecord.startTransaction();
  try {
    await new Promise((resolve, reject) => {
      let hasEnded = false;
      let readStream = fs.createReadStream(path);
      readStream
        .pipe(csvParser())
        .on('data', async (data) => {
          try {
            await SalesRecord.query(trx).insert(normalizeRow(data));
            if (hasEnded) {
              resolve();
            }
          } catch (e) {
            readStream.destroy();
            reject();
          }
        })
        .on('end', () => {
          // do not resolve here as there can be SalesRecord still being inserted
          hasEnded = true;
        });
    });
    await trx.commit();
  } catch (e) {
    await trx.rollback();
    throw new Error('Insertion failed');
  }
};

const normalizeRow = (row) => {
  row = {
    userName: row['USER_NAME'],
    age: row['AGE'],
    height: row['HEIGHT'],
    gender: row['GENDER'].toLowerCase(),
    saleAmount: row['SALE_AMOUNT'],
    lastPurchaseDate: moment(row['LAST_PURCHASE_DATE']).toDate(),
  };
  return row;
};
