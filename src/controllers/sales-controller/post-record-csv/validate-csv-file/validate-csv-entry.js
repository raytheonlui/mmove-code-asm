let moment = require('moment');

module.exports = (row) => {
  const {
    USER_NAME: userName,
    AGE: age,
    HEIGHT: height,
    GENDER: gender,
    SALE_AMOUNT: saleAmount,
    LAST_PURCHASE_DATE: lastPurchaseDate,
  } = row;

  if (!userName) return 'Missing USER_NAME';

  if (!age) return 'Missing AGE';
  if (isNaN(age)) return 'AGE must be an integer';
  if (age != parseInt(age)) return 'AGE must be an integer';
  if (age < 0) return 'AGE must be an non-negative';

  if (!height) return 'Missing HEIGHT';
  if (isNaN(height)) return 'HEIGHT must be an integer';
  if (height != parseInt(height)) return 'HEIGHT must be an integer';

  if (!gender) return 'Missing GENDER';
  if (!/^[mf]$/i.test(gender)) return 'GENDER must be either "m" or "f"';

  if (!saleAmount) return 'Missing SALE_AMOUNT';
  if (isNaN(saleAmount)) return 'SALE_AMOUNT must be an integer';
  if (saleAmount != parseInt(saleAmount)) return 'SALE_AMOUNT must be an integer';

  if (!lastPurchaseDate) return 'Missing LAST_PURCHASE_DATE';
  if (!moment(lastPurchaseDate , moment.ISO_8601).isValid()) return 'LAST_PURCHASE_DATE must be in ISO 8601 Format';

  return null;
};
