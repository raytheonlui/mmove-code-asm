const fs = require('fs-extra');
const csvParser = require('csv-parser');
const validateCsvEntry = require('./validate-csv-entry');

module.exports = (path) =>
  new Promise((resolve, reject) => {
    try {
      let lineNumber = 0;
      let readStream = fs.createReadStream(path);
      readStream
        .pipe(csvParser())
        .on('data', (data) => {
          lineNumber++;
          let error = validateCsvEntry(data);
          if (error) {
            reject(`Validation Error on line ${lineNumber} : ${error}`);
            readStream.destroy();
          }
        })
        .on('end', () => {
          resolve();
        });
    } catch (e) {
      reject(e);
    }
  });
