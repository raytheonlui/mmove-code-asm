const record = require('./post-record-csv');
const report = require('./get-report');

module.exports = {
  record,
  report,
};
