const moment = require('moment');

const SalesRecord = require('../../../models/SalesRecord');
const validateQueryStirng = require('./validate-query-string');

module.exports = async (req, res) => {
  let { page = 1, size = 20, fromDate, toDate, onDate } = req.query;

  let validationError = validateQueryStirng({ page, size, fromDate, toDate, onDate });
  if (validationError) {
    res.status(401);
    res.send({
      ok: false,
      message: validationError,
    });
    return;
  }

  let query = SalesRecord.query()
    .limit(size)
    .offset(size * (page - 1));

  if (fromDate) {
    query = query.andWhere('lastPurchaseDate', '>=', moment(`${fromDate}T00:00:00.000Z`).toDate());
  }

  if (toDate) {
    query = query.andWhere('lastPurchaseDate', '<=', moment(`${toDate}T23:59:59.999Z`).toDate());
  }

  if (onDate) {
    query = query.andWhere('lastPurchaseDate', '>=', moment(`${onDate}T00:00:00.000Z`).toDate());
    query = query.andWhere('lastPurchaseDate', '<=', moment(`${onDate}T23:59:59.999Z`).toDate());
  }

  let salesRecords;

  try {
    salesRecords = await query;
  } catch (e) {
    res.status(500);
    res.send({
      ok: false,
      message: 'query failed',
    });
    return;
  }

  res.status(200);
  res.send({
    ok: true,
    salesRecords,
  });
};
