const moment = require('moment');

module.exports = ({ page, size, fromDate, toDate, onDate }) => {
  if (page <= 0) {
    return 'page must be positive';
  }

  if (size <= 0) {
    return 'size must be positive';
  }

  if (fromDate && !moment(fromDate, 'YYYY-MM-DD', true).isValid()) {
    return 'invalid value for fromDate';
  }

  if (toDate && !moment(toDate, 'YYYY-MM-DD', true).isValid()) {
    return 'invalid value for toDate';
  }

  if (onDate && !moment(onDate, 'YYYY-MM-DD', true).isValid()) {
    return 'invalid value for onDate';
  }

  if (onDate && (fromDate || toDate)){
    return 'onDate cannot be used together with fromDate and toDate';
  }

  return null;
};
