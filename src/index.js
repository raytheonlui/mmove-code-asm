const startExpress = require('./startups/start-express');
const connectMysqlDatabase = require('./startups/connect-mysql-databbase');
const startSchedules = require('./startups/start-schedules');

require('dotenv').config();

(async () => {
  try {
    await connectMysqlDatabase({
      host: process.env.MYSQL_HOST,
      port: process.env.MYSQL_PORT,
      database: process.env.MYSQL_DATABASE,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
    });
  } catch (e) {
    console.error('Cannot Connection to MySQL DB');
    process.exit(1);
  }

  startExpress({ port: process.env.PORT });

  startSchedules({externalEndpointUrl: process.env.EXTERNAL_ENDPOINT_URL});
})();
