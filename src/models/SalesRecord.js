const { Model, snakeCaseMappers } = require('objection');

class SalesRecord extends Model {
  static get tableName() {
    return 'sales_record';
  }

  static get columnNameMappers() {
    return snakeCaseMappers();
  }
}

module.exports = SalesRecord;
