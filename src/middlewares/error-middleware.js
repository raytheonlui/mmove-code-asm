module.exports = async (err, req, res, next) => {
  if (err) {
    console.error(err);
    res.status(500);
    res.send({ ok: false, message: 'server error' });
  }
};
