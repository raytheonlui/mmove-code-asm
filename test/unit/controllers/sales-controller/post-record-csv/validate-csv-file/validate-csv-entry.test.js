/* eslint-disable no-undef */
const func = require('../../../../../../src/controllers/sales-controller/post-record-csv/validate-csv-file/validate-csv-entry');

const goodEntry = {
  USER_NAME: 'John Doe',
  AGE: 29,
  HEIGHT: 177,
  GENDER: 'M',
  SALE_AMOUNT: 21312,
  LAST_PURCHASE_DATE: '2020-11-05T13:15:30Z',
};

describe('validate-csv-entry', () => {
  it('returns null for good entries', () => {
    let result = func(goodEntry);
    expect(result).toBe(null);
  });

  it('returns error message if USER_NAME is missing', () => {
    let result = func({ ...goodEntry, USER_NAME: undefined });
    expect(result).not.toBe(null);
  });

  it('returns error message if AGE is missing', () => {
    let result = func({ ...goodEntry, AGE: undefined });
    expect(result).not.toBe(null);
  });

  it('returns error message if AGE is not a number', () => {
    let result = func({ ...goodEntry, AGE: 'abc' });
    expect(result).not.toBe(null);
  });

  it('returns error message if AGE is not an integer', () => {
    let result = func({ ...goodEntry, AGE: 12.3 });
    expect(result).not.toBe(null);
  });

  it('returns error message if AGE is negative', () => {
    let result = func({ ...goodEntry, AGE: -30 });
    expect(result).not.toBe(null);
  });

  it('returns error message if HEIGHT is missing', () => {
    let result = func({ ...goodEntry, HEIGHT: undefined });
    expect(result).not.toBe(null);
  });

  it('returns error message if HEIGHT is not a number', () => {
    let result = func({ ...goodEntry, HEIGHT: 'abc' });
    expect(result).not.toBe(null);
  });

  it('returns error message if HEIGHT is not an integer', () => {
    let result = func({ ...goodEntry, HEIGHT: 12.3 });
    expect(result).not.toBe(null);
  });

  it('returns error message if GENDER is missing', () => {
    let result = func({ ...goodEntry, GENDER: undefined });
    expect(result).not.toBe(null);
  });

  it('returns error message if GENDER is not m or f (case insensitive)', () => {
    let result = func({ ...goodEntry, GENDER: 'a' });
    expect(result).not.toBe(null);
  });

  it('returns error message if SALE_AMOUNT is missing', () => {
    let result = func({ ...goodEntry, SALE_AMOUNT: undefined });
    expect(result).not.toBe(null);
  });

  it('returns error message if SALE_AMOUNT is not a number', () => {
    let result = func({ ...goodEntry, SALE_AMOUNT: 'abc' });
    expect(result).not.toBe(null);
  });

  it('returns error message if SALE_AMOUNT is not an integer', () => {
    let result = func({ ...goodEntry, SALE_AMOUNT: 12.3 });
    expect(result).not.toBe(null);
  });

  it('returns error message if LAST_PURCHASE_DATE is missing', () => {
    let result = func({ ...goodEntry, LAST_PURCHASE_DATE: undefined });
    expect(result).not.toBe(null);
  });

  it('returns error message if LAST_PURCHASE_DATE is not in ISO 8601 Format', () => {
    let result = func({ ...goodEntry, LAST_PURCHASE_DATE: '11/05/2020 13:15:30' });
    expect(result).not.toBe(null);
  });
});
