/* eslint-disable no-undef */
const func = require('../../../../../src/controllers/sales-controller/get-report/validate-query-string');

const goodEntry = {
  page: 1,
  size: 20,
  fromDate: '2021-01-01',
  toDate: '2021-01-31',
};

describe('validate-query-string', () => {
  it('returns null for good entries', () => {
    let result = func(goodEntry);
    expect(result).toBe(null);
  });

  it('returns null for empty entries', () => {
    let result = func({});
    expect(result).toBe(null);
  });

  it('returns error message if page is non-positive', () => {
    let result = func({ ...goodEntry, page: 0 });
    expect(result).not.toBe(null);
  });

  it('returns error message if size is non-positive', () => {
    let result = func({ ...goodEntry, size: 0 });
    expect(result).not.toBe(null);
  });

  it('returns error message if fromDate is not in YYYY-MM-DD format', () => {
    let result = func({ ...goodEntry, fromDate: '01/01/2021' });
    expect(result).not.toBe(null);
  });

  it('returns error message if fromDate is not valid', () => {
    let result = func({ ...goodEntry, fromDate: '2021-02-30' });
    expect(result).not.toBe(null);
  });

  it('returns error message if toDate is not in YYYY-MM-DD format', () => {
    let result = func({ ...goodEntry, toDate: '01/01/2021' });
    expect(result).not.toBe(null);
  });

  it('returns error message if toDate is not valid', () => {
    let result = func({ ...goodEntry, toDate: '2021-02-30' });
    expect(result).not.toBe(null);
  });

  it('returns error message if onDate is used together with fromDate', () => {
    let result = func({ ...goodEntry, toDate:undefined , onDate: '2021-01-15' });
    expect(result).not.toBe(null);
  });

  it('returns error message if onDate is used together with toDate', () => {
    let result = func({ ...goodEntry, fromDate:undefined , onDate: '2021-01-15' });
    expect(result).not.toBe(null);
  });

  it('returns error message if onDate is not in YYYY-MM-DD format', () => {
    let result = func({ ...goodEntry, fromDate: undefined, toDate: undefined, onDate: '01/01/2021' });
    expect(result).not.toBe(null);
  });

  it('returns error message if onDate is not valid', () => {
    let result = func({ ...goodEntry, fromDate: undefined, toDate: undefined, onDate: '2021-02-30' });
    expect(result).not.toBe(null);
  });
});
