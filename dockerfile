FROM node:10.15.3-alpine

WORKDIR /var/app

COPY package*.json ./
RUN npm ci --only=production

COPY src src

CMD ["node", "src/index.js"]