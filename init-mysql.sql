CREATE TABLE IF NOT EXISTS `mmove_code_asm`.`sales_record` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(128) NOT NULL,
  `age` INT NOT NULL,
  `height` INT NOT NULL,
  `gender` VARCHAR(1) NOT NULL,
  `sale_amount` INT NOT NULL,
  `last_purchase_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`));
